package com.sheetal.service;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sheetal.dto.FlightAcknowldgement;
import com.sheetal.dto.FlightBookingRequest;
import com.sheetal.entity.PassengerInfo;
import com.sheetal.entity.PaymentInfo;
import com.sheetal.repository.PasssengerRepository;
import com.sheetal.repository.PaymentRepository;
import com.sheetal.util.PaymentValidation;

@Service
public class FlightbookingService {
	@Autowired
	private PasssengerRepository passsengerRepository;
	@Autowired
	private PaymentRepository paymentRepository;

	public FlightAcknowldgement getdetails(FlightBookingRequest flightBookingRequest) {
		PassengerInfo passengerInfo = flightBookingRequest.getPassengerInfo();
		passengerInfo.setAge(25);
		passsengerRepository.save(passengerInfo);

		PaymentInfo paymentInfo = flightBookingRequest.getPaymentInfo();
		PaymentValidation.validateCreditLimit(paymentInfo.getAccountNo(), passengerInfo.getFare());

		paymentInfo.setPassengerId(passengerInfo.getId());
		paymentInfo.setAmount(passengerInfo.getFare());
		paymentRepository.save(paymentInfo);

		return new FlightAcknowldgement("SUCCESS", UUID.randomUUID().toString().split("-")[0], passengerInfo.getFare(),
				passengerInfo);

	}

}
