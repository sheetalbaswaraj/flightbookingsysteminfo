package com.sheetal.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sheetal.entity.PaymentInfo;

public interface PaymentRepository extends JpaRepository<PaymentInfo, String>{

}
