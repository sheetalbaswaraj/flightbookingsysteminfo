package com.sheetal.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sheetal.entity.PassengerInfo;

public interface PasssengerRepository extends JpaRepository<PassengerInfo, Long> {

}
