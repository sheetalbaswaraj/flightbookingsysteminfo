package com.sheetal.exception;

public class InsufficientFund extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public InsufficientFund(String message) {
		super(message);
	}

}
