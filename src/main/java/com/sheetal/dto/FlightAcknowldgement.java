package com.sheetal.dto;

import com.sheetal.entity.PassengerInfo;

public class FlightAcknowldgement {

	private String status;
	private String pnr;
	private double fare;
	private PassengerInfo passengerInfo;

	public FlightAcknowldgement(String status, String pnr, double fare, PassengerInfo passengerInfo) {
		super();
		this.status = status;
		this.pnr = pnr;
		this.fare = fare;
		this.passengerInfo = passengerInfo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public double getFare() {
		return fare;
	}

	public void setFare(double fare) {
		this.fare = fare;
	}

	public PassengerInfo getPassengerInfo() {
		return passengerInfo;
	}

	public void setPassengerInfo(PassengerInfo passengerInfo) {
		this.passengerInfo = passengerInfo;
	}

}
