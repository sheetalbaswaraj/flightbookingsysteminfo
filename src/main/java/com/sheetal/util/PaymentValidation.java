package com.sheetal.util;

import java.util.HashMap;
import java.util.Map;

import com.sheetal.exception.InsufficientFund;

public class PaymentValidation {

	private static Map<String, Double> paymentMap = new HashMap<>();

	static {
		paymentMap.put("SBI_CARD", 23500.0);
		paymentMap.put("CANARA_BANK", 11200.0);
		paymentMap.put("STANDARD_CHARTERED", 22200.0);
	}

	public static boolean validateCreditLimit(String accountNo, double fare) {
		if (fare > paymentMap.get(accountNo)) {
			throw new InsufficientFund("Insufficientfund..!!");
		} else {

			return true;
		}
	}
}
