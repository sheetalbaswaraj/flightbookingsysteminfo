package com.sheetal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sheetal.dto.FlightAcknowldgement;
import com.sheetal.dto.FlightBookingRequest;
import com.sheetal.service.FlightbookingService;

@RestController
public class Flightbookingcontroller {

	@Autowired
	private FlightbookingService flightbookingservice;

	@PostMapping("/bookflightTicket")
	public FlightAcknowldgement getDetails(@RequestBody FlightBookingRequest flightBookingRequest) {
		return flightbookingservice.getdetails(flightBookingRequest);

	}
}
